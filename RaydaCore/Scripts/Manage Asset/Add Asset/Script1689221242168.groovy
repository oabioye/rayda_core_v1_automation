import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.io.*
import java.util.Random as Random

Random rand = new Random()

int serialNo = rand.nextInt(400)

WebUI.callTestCase(findTestCase('Login/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/h3_Hello Obaloluwa'), 'Hello Obaloluwa!')

WebUI.click(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/a_See all assets'))

WebUI.click(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/button_Add asset'))

WebUI.setText(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/input_Serial Number_serial_number'), GlobalVariable.SerialNumber)

WebUI.callTestCase(findTestCase('Manage Asset/enter_asset_details'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/input_Cancel_smallModal'))

try {
    WebUI.verifyTextPresent('The asset ID has already been used', false)

    GlobalVariable.SerialNumber = (GlobalVariable.SerialNumber + serialNo.toString())

    WebUI.scrollToElement(findTestObject('AddAsset_OR/Page_Rayda/input_Serial Number_serial_number'), 60)

    WebUI.setText(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/input_Serial Number_serial_number'), GlobalVariable.SerialNumber)

    WebUI.callTestCase(findTestCase('Manage Asset/enter_asset_details'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/input_Cancel_smallModal'), 60)

    WebUI.click(findTestObject('Object Repository/AddAsset_OR/Page_Rayda/input_Cancel_smallModal'))
}
catch (Exception e) {
    println(e.toString())
} 

WebUI.verifyTextPresent(GlobalVariable.SerialNumber, false)

WebUI.callTestCase(findTestCase('Login/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

